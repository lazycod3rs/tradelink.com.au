import {useState} from 'react'
import axios from 'axios'
import NProgress from 'nprogress'
import { useToasts } from 'react-toast-notifications'
import Link from 'next/link'

export default function Home() {
    const [url, setUrl] = useState('');
    const [text, setText] = useState('');
    const { addToast } = useToasts();

    const scrapping = async (e) => {
        e.preventDefault();
        NProgress.start();
        try{
            const {data} = await axios.get('/api/scrap', {params: {url}});
            setText(data);
            addToast('Scrapped Successfully!', { appearance: 'success', autoDismiss: true })
        }catch(e){
            console.log(e);
            addToast('Something Wrong!', { appearance: 'error', autoDismiss: true })
        }
        NProgress.done();
    }

    const copyToClipboard = (e) =>{
        const el = document.createElement('textarea');
        el.value = text;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        addToast('Copied to Clipboard!', { appearance: 'success', autoDismiss: true })
    }

    return (
        <>
            <div className="top-bar">
                <div className="top-bar-left">
                    <ul className="menu">
                      <li><Link href="/"><a>Scrap</a></Link></li>
                      <li><Link href="/xlsx"><a >XLSX Scrap</a></Link></li>
                    </ul>
                </div>
            </div>


            <div className='grid-container' style={{marginTop: 20}}>
                <h3 className="text-center" style={{marginBottom: 30}}>Scrapper for TradeLink</h3>
                <form onSubmit={scrapping} style={{marginBottom: 40}}>
                    <div className="grid-x grid-margin-x align-center">
                        <div className="cell medium-6">
                            <input type="url" value={url} onChange={(e)=>setUrl(e.target.value)} placeholder="Paste URL here and hit Scrap"/>
                        </div>
                        <div className="cell shrink">
                            <button type='submit' className='button' >Scrap</button>
                        </div>
                        <div className="cell shrink">
                            <button type='button' className='button' onClick={()=>{setUrl(''); setText('')}} >Clear</button>
                        </div>
                    </div>
                </form>
                <div className="text-center">Click anywhere on the below box to copy</div>
                <textarea cols="30" rows="24" defaultValue={text} onClick={copyToClipboard} />
            </div>
        </>
    )
}

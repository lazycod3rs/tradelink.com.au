import { ToastProvider } from 'react-toast-notifications'

import 'foundation-sites/dist/css/foundation.min.css';
import 'nprogress/nprogress.css';
import '../styles/app.css'

function MyApp({ Component, pageProps }) {
  return <ToastProvider><Component {...pageProps} /></ToastProvider>
}

export default MyApp

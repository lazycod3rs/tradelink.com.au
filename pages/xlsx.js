import {useState} from 'react'
import axios from 'axios'
import NProgress from 'nprogress'
import { useToasts } from 'react-toast-notifications'
import Link from 'next/link'
import readXlsxFile from 'read-excel-file'

const XLSX = () =>{
    const [text, setText] = useState('');
    const [total, setTotal] = useState(0);
    const [totalScrapped, setTotalScrapped] = useState(0);
    const { addToast } = useToasts();
    const [download, setDownload] = useState('');

    let temp = '';
    let ts = 0;
    let textFile = null;

    const makeFile = () =>{
        var data = new Blob([text], {type: 'text/plain'});

        // If we are replacing a previously generated file we need to
        // manually revoke the object URL to avoid memory leaks.
        if (textFile !== null) {
          window.URL.revokeObjectURL(textFile);
        }

        textFile = window.URL.createObjectURL(data);

        setDownload(textFile);
    }

    const handleChange = async (e) =>{
        NProgress.start();
        let file = e.target.files[0];
        let t = 0;

        try{
            let rows = await readXlsxFile(file);
            for(let i = 0; i < rows.length; i++){
                var re = new RegExp("^(http|https)://", "i");
                if (re.test(rows[i][0])) {
                    console.log(rows[i][0]);
                    let url = rows[i][0];
                    try{
                        const {data} = await axios.get('/api/scrap', {params: {url}});
                        temp += data;
                        ts++;
                        setTotalScrapped(ts);
                        makeFile();
                        addToast(`${url} Done!`, { appearance: 'success', autoDismiss: true })
                    }catch(e){
                        console.log(e);
                        addToast('Something Wrong!', { appearance: 'error', autoDismiss: true })
                    }

                    t++;
                }
            }
        }catch(err){
            console.log(err)
        }

        setText(temp);
        setTotal(t);
        NProgress.done();
    }

    const copyToClipboard = (e) =>{
        const el = document.createElement('textarea');
        el.value = text;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        addToast('Copied to Clipboard!', { appearance: 'success', autoDismiss: true })
    }

    return (
        <>
            <div className="top-bar">
                <div className="top-bar-left">
                    <ul className="menu">
                      <li><Link href="/"><a>Scrap</a></Link></li>
                      <li><Link href="/xlsx"><a >XLSX Scrap</a></Link></li>
                    </ul>
                </div>
            </div>
            <div className='grid-container' style={{marginTop: 20}}>
                <h3 className="text-center" style={{marginBottom: 30}}>Scrapper for bunnings.com.aune</h3>
                <input type="file" onChange={handleChange}/>
                <div className="grid-x grid-margin-x">
                    <div className="cell shrink">
                        Total: {total}
                    </div>
                    <div className="cell shrink">
                        Scrapped: {totalScrapped}
                    </div>
                </div>
                <br/>
                <textarea cols="30" rows="24" defaultValue={text} onClick={copyToClipboard} />
            </div>
        </>
    )
}

export default XLSX;

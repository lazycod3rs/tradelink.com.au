import axios from 'axios'
import cheerio from 'cheerio'

export default (req, res) => {
    (async()=>{
        try{
            let response = await axios.get(req.query.url);
            let $ = cheerio.load(response.data);
            //let title = $('.main-page-header').text().trim();
            let model = $('dl.eTfNUs dd:nth-child(4)').text().trim();
            let descList = $('dl.product-detail-description .bui-body p').html().split('<br>');
            let desc = [];
            for (let i=0; i < descList.length; i++) {
                if(descList[i].trim() != '') {
                    desc.push(descList[i].trim());

                }
            }
            let overview = $('dl.product-detail-description .bui-body ul li');
            let spec = $('#accordion-specifications-body tbody tr');
            let brand = $('dl.eTfNUs dd:last-child').text().trim();

            let text = `===========================================================\n`;
            text += `${req.query.url}\n`;
            text += `Title: \n`;
            text += `Price: $\n`;
            text += `\n`;
            // text+= `${desc}`;
            for (let j=0; j < desc.length; j++){
                text += desc[j] + `\n \n`;
            }
            text += `\n`;
            text += `Overview\n`;
            text += `\n`;

            overview.each((i, e)=>{
                text += `o ${$(e).text().trim()}\n`
            });
            text += `\n`;
            text += `\n`;
            text += `Specifications\n`;
            text += `\n`;
            text += `o Contents:\n`;
            text += `\n`;
            text += `\n`;
            text += `o Model Number: ${model}\n`;


            spec.each((i, e)=>{
                text += `o ${$(e).children(":first-child").text().trim()}: ${$(e).children(":last-child").text().trim()}\n`;
            });

            text += `o Brand Name: ${brand}\n`;

            text += `........................................................................................................................................\n`;
            text += `\n`;
            text += `Note: This is a Special Order Item and will take approximately 2 weeks to order.\n`;
            text += `\n`;
            text += `Note: Photo for illustrative purposes only.\n`;

            res.status(200).json(text);
            console.log("Testing")
        }catch(err){
            console.log(err);
        }
    })()

}
